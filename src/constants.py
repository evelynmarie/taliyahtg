#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import subprocess

version = "0.7.0"
name = "Taliyah"
revision = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip().decode("utf8")
