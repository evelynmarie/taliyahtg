#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

from psutil import __version__ as psutilver
from requests import __version__ as reqsver
from telegram import Update
from telegram import __version__ as tgver
from telegram.constants import ChatAction, ParseMode
from telegram.ext import ContextTypes


async def libraries(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    Plugin for sending a message containing the libraries the bot uses.
    """
    name = context.bot.first_name
    await update.message.chat.send_action(action=ChatAction.TYPING)
    await update.message.reply_text(
        parse_mode=ParseMode.MARKDOWN,
        text=f"*{name}* runs on a number of libraries. The names and versions of the libraries we use are listed below.\n\n"
        f"*python-telegram-bot*: {tgver}\n"
        f"*requests*: {reqsver}\n"
        f"*psutil*: {psutilver}",
    )
