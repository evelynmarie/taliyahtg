#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import json

import requests
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.constants import ChatAction, ParseMode


async def xkcd(update, context) -> None:
    """xkcd plugin.

    This plugin retrieves an image from xkcd as well as the image caption, and
    shows links to an explanation for the image on explainxkcd, as well as a
    link to the primary xkcd page.

    Args:
        num: integer for xkcd image
    """
    base_url = "https://xkcd.com/info.0.json"
    strip_url = "https://xkcd.com/%s/info.0.json"

    try:
        num: int = context.args[0]
        xkcd = requests.get(strip_url % num, timeout=10).text
        xkcd = json.loads(xkcd)

        caption = f"{xkcd['title']} - {xkcd['alt']}"
        keyboard = [
            [
                InlineKeyboardButton("View explanation", url=f"https://www.explainxkcd.com/wiki/index.php/{num}"),
                InlineKeyboardButton("View image page", url=f"https://xkcd.com/{num}"),
            ]
        ]

        await update.message.chat.send_action(action=ChatAction.UPLOAD_PHOTO)
        await update.message.reply_photo(photo=xkcd["img"], caption=caption, reply_markup=InlineKeyboardMarkup(keyboard))
    except ValueError:
        await update.message.chat.send_action(action=ChatAction.TYPING)
        await update.message.reply_text(parse_mode=ParseMode.MARKDOWN, text="Invalid image ID provided. Example ID: 378")
    except IndexError:
        xkcd = requests.get(base_url, timeout=10).text
        xkcd = json.loads(xkcd)

        num = xkcd["num"]
        caption = f"{xkcd['title']} - {xkcd['alt']}"
        keyboard = [
            [
                InlineKeyboardButton("View description", url=f"https://www.explainxkcd.com/wiki/index.php/{num}"),
                InlineKeyboardButton("View image page", url=f"https://xkcd.com/{num}"),
            ]
        ]

        await update.message.chat.send_action(action=ChatAction.UPLOAD_PHOTO)
        await update.message.reply_photo(photo=xkcd["img"], caption=caption, reply_markup=InlineKeyboardMarkup(keyboard))
